import React from "react";
import PropTypes from "prop-types";

const Button = ({ children, onClick, ...rest }) => {
  return (
    <button onClick={onClick} {...rest} data-testid='testButton'>
      {children}
    </button>
  );
};

export default Button;

Button.defaultProps = {
    children: '',
}

Button.propTypes = {
    onClick: PropTypes.func,
    rest: PropTypes.array
}