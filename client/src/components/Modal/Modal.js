import React from "react";
import Button from "../Button/Button";
import PropTypes from "prop-types";
import "./Modal.scss";
import { connect } from "react-redux";
import { closeModalOperation } from "../../store/operations/homeOperations";

export const Modal = ({ modalConfig, closeModal }) => {
  return (
    <div className="modal" data-testid="testModal" onClick={() => closeModal()}>
      <div className="modal__content" onClick={(e) => e.stopPropagation()}>
        {!!modalConfig.close && (
          <Button onClick={() => closeModal()} className="modal__close">
            &#10006;
          </Button>
        )}
        <div className="modal__header">{modalConfig.header}</div>
        <div className="modal__body" >{modalConfig.body}</div>
        <div className="modal__footer">{modalConfig.footer}</div>
      </div>
    </div>
  );
};

Modal.defaultProps = {
  modalConfig: {
    header: "Header",
    body: "Body",
    footer: "Footer",
    close: false,
  },
};

Modal.propTypes = {
  modalConfig: PropTypes.object,
};

const mapStateToProps = (state) => {
  return {
    isActiveModal: state.cart.isActiveModal,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    closeModal: () => {
      console.log('Inside Close Modal');
      dispatch(closeModalOperation())
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Modal);
