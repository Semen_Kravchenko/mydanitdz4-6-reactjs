import { Modal } from "./Modal";
import { fireEvent, render } from "@testing-library/react";

describe("Testing Modal.js", () => {
  test("Smoke test of Modal", () => {
    render(<Modal />);
  });
  test("Testing Modal header", () => {
    const { getByText, rerender } = render(<Modal />);
    const modalHeader = getByText(/Header/i);
    const testHeaderStr = "Test Header";
    rerender(<Modal modalConfig={{ header: testHeaderStr }} />);
    expect(modalHeader).toBeInTheDocument();
    expect(modalHeader).toHaveTextContent(testHeaderStr);
  });
  test("Testing Modal body", () => {
    const { getByText, rerender } = render(<Modal />);
    const modalBody = getByText(/Body/i);
    const testBodyStr = "Test Body";
    rerender(<Modal modalConfig={{ body: testBodyStr }} />);
    expect(modalBody).toBeInTheDocument();
    expect(modalBody).toHaveTextContent(testBodyStr);
  });
  test("Testing Modal footer", () => {
    const { getByText, rerender } = render(<Modal />);
    const modalFooter = getByText(/footer/i);
    const testFooterStr = "Test Footer";
    rerender(<Modal modalConfig={{ footer: testFooterStr }} />);
    expect(modalFooter).toBeInTheDocument();
    expect(modalFooter).toHaveTextContent(testFooterStr);
  });
  test("Сlicking the area without content Modal should be closed", () => {
    const closeModalMock = jest.fn();
    const { getByTestId } = render(<Modal closeModal={closeModalMock} />);
    const closeModal = getByTestId(/testModal/i);
    expect(closeModal).toBeInTheDocument();

    expect(closeModalMock).not.toHaveBeenCalled();
    fireEvent.click(closeModal);
    expect(closeModalMock).toHaveBeenCalledTimes(1);
  });
});
