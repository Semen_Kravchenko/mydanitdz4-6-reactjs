import React from "react";
import "./AddedCardsToShow.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";

const AddedCardsToShow = ({
  label,
  id,
  title,
  author,
  artNum,
  imgSrc,
  price,
  removeCard,
  quantity,
  subQuantity,
  addQuantity,
}) => {
  return (
    <li
      className={`added-card added-card-#${artNum}`}
      key={`added-card-to-${label}-${id}`}
    >
      <Button className="added-card__remove" onClick={() => removeCard(id)}>
        &#10060;
      </Button>
      <div className="added-card__img-wrapper">
        <img
          className="added-card__img"
          src={imgSrc}
          alt={`added-card-#${artNum}`}
        />
      </div>
      <div className="added-card__content">
        <div className="added-card__content-left">
          <p className="added-card__title">{title}</p>
          <p className="added-card__author">by {author}</p>
          <p className="added-card__artNum">art#{artNum}</p>
        </div>
        <div className="added-card__content-right">
          {!!quantity && (
            <div className="added-card__quantity-wrapper">
              <Button
                className="added-card__quantity-sub"
                onClick={() => subQuantity(id)}
              >
                &#8211;
              </Button>
              <span className="added-card__quantity-count">{quantity}</span>
              <Button
                className="added-card__quantity-add"
                onClick={() => addQuantity(id)}
              >
                &#43;
              </Button>
            </div>
          )}
          <p className="added-card__price">${price}</p>
        </div>
      </div>
    </li>
  );
};

AddedCardsToShow.propTypes = {
  label: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  artNum: PropTypes.number.isRequired,
  imgSrc: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  removeCard: PropTypes.func.isRequired,
  quantity: PropTypes.number,
  subQuantity: PropTypes.func,
  addQuantity: PropTypes.func,
};

export default AddedCardsToShow;
