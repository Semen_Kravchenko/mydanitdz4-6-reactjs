import React from "react";
import PropTypes from "prop-types";
import AddToCartIcon from "../../theme/icons/AddToCartIcon";
import AddToWishListIcon from "../../theme/icons/AddToWishListIcon";
import RemoveFromCartIcon from "../../theme/icons/RemoveFromCartIcon";
import "./CardsToShow.scss";
import Button from "../Button/Button";

const CardsToShow = ({
  id,
  title,
  author,
  artNum,
  price,
  imgSrc,
  addToCart,
  addToWishList,
  addedCardsFromCart,
  addedCardsFromWishList,
}) => {
  const checkedCardFromCart =
    !!addedCardsFromCart && addedCardsFromCart.find((e) => e.id === id);
  const checkedCardFromWishList =
    !!addedCardsFromWishList && addedCardsFromWishList.find((e) => e.id === id);

  return (
    <li className={`card card-#${artNum}`} key={`card-${id}`}>
      <div className="card__img-wrapper">
        <img className="card__img" src={imgSrc} alt={`card-#${artNum}`} />
        {!!addToWishList && (
          <Button
            className="card__wish-button"
            onClick={() => addToWishList(id)}
          >
            <AddToWishListIcon
              width="27px"
              fill={!!checkedCardFromWishList ? "gold" : "transparent"}
              stroke="gold"
            />
          </Button>
        )}

        {!!addToCart && (
          <Button className="card__buy-button" onClick={() => addToCart(id)}>
            {!!checkedCardFromCart ? (
              <RemoveFromCartIcon width="30px" fill="tomato" />
            ) : (
              <AddToCartIcon width="30px" fill="#AFF4AD" />
            )}
          </Button>
        )}
      </div>
      <div className="card__content">
        <p className="card__title">{title}</p>
        <p className="card__author">by {author}</p>
        <p className="card__artnum">art#{artNum}</p>
        <p className="card__price">${price}</p>
      </div>
    </li>
  );
};

CardsToShow.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  artNum: PropTypes.number.isRequired,
  imgSrc: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  addToCart: PropTypes.func,
  addToWishList: PropTypes.func,
  addedCardsFromCart: PropTypes.array,
  addedCardsFromWishList: PropTypes.array,
};

export default CardsToShow;
