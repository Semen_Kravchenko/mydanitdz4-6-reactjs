import "./Loader.scss";

const Loader = () => {
  return (
    <div className="loader__wrapper">
      <div className="loader" />
    </div>
  );
};

export default Loader;
