import * as yup from "yup";
import "yup-phone";

const FIELD_REQUIRED = "This field is required";

const schema = yup.object().shape({
  firstName: yup.string().required(FIELD_REQUIRED).trim().min(2, "Must be longer than 2 characters"),
  lastName: yup.string().required(FIELD_REQUIRED).trim().min(2, "Must be longer than 2 characters"),
  age: yup.number().required(FIELD_REQUIRED).integer("Age cannot include a decimal point").max(100),
  address: yup.string().required(FIELD_REQUIRED).trim().min(2, "Must be longer than 2 characters"),
  tel: yup.string().required(FIELD_REQUIRED).phone('UA', true, 'This is not a valid phone number')
});

export default schema;
