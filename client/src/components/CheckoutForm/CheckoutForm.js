import { withFormik } from "formik";
import { connect } from "react-redux";
import React from "react";
import PropTypes from 'prop-types';
import './CheckoutForm.scss';
import Input from "./Input/Input";
import schema from "./schema";
import {
  cancelCheckOutOperation,
  closeCheckOutOperation,
} from "../../store/operations/cartOperations";
import Button from "../Button/Button";

const CheckoutForm = ({ cancelCheckOut, handleSubmit, isSubmitting }) => {
  return (
    <section className="checkout-form">
      <div className="container">
        <div className="checkout-form__inner">
          <form onSubmit={handleSubmit} noValidate>
            <h2>Checkout Form</h2>
            <Input name="firstName" labelName='First Name' type="text" placeholder="e.g.: Taras" />
            <Input name="lastName" labelName='Last Name' type="text" placeholder="e.g.: Shevchenko" />
            <Input name="age" labelName='Age' type="number" placeholder="e.g.: 18" min="1" />
            <Input
              name="address"
              labelName='Address'
              type="text"
              placeholder="e.g.: Khreshchatyk, 15"
            />
            <Input name="tel" labelName='Phone Number' type="tel" placeholder="e.g.: 0999999999" />
            <div className='checkout-form__buttons-wrapper'>
              <Button className='checkout-form__buttons checkout-form__checkout-button' disabled={isSubmitting} type="submit">
                Checkout
              </Button>
              <Button className='checkout-form__buttons checkout-form__cancel-button' type="button" onClick={() => cancelCheckOut(false)}>
                Cancel
              </Button>
            </div>
          </form>
        </div>
      </div>
    </section>
  );
};

const checkout = (values, { props, setSubmitting }) => {
  console.log("Customer info: ", values);
  props.closeCheckOut(false);
  setSubmitting(false);
};

const mapDispatchToProps = (dispatch) => {
  return {
    closeCheckOut: (bool) => dispatch(closeCheckOutOperation(bool)),
    cancelCheckOut: (bool) => dispatch(cancelCheckOutOperation(bool)),
  };
};

CheckoutForm.propTypes = {
  cancelCheckOut: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  isSubmitting: PropTypes.bool.isRequired,
}

export default connect(
  null,
  mapDispatchToProps
)(
  withFormik({
    mapPropsToValues: () => ({
      firstName: "",
      lastName: "",
      age: "",
      address: "",
      tel: "",
    }),
    handleSubmit: checkout,
    validationSchema: schema,
  })(CheckoutForm)
);
