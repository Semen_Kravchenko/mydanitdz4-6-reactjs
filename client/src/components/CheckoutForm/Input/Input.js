import { useField } from "formik";
import React from "react";
import PropTypes from "prop-types";
import "./Input.scss";

const Input = ({ name, labelName, ...rest }) => {
  const [field, meta /*helpers*/] = useField(name);
  return (
    <div className={`form-input input-for-${name}`}>
      <label className="form-input__label" htmlFor="form-input">
        {`${labelName}:`}
        <input
          id="form-input"
          className="form-input__field"
          {...field}
          {...rest}
        />
      </label>
      {meta.error && meta.touched && (
        <span className="form-input__error-mg">{meta.error}</span>
      )}
    </div>
  );
};

Input.propTypes = {
  name: PropTypes.string.isRequired,
  labelName: PropTypes.string.isRequired,
  rest: PropTypes.arrayOf(PropTypes.string),
};

export default Input;
