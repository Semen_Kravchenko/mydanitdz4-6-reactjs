import React from "react";
import "./Navbar.scss";
import { Link } from "react-router-dom";
import BillboardIcon from "../../theme/icons/BillboardIcon";
import CartIcon from "../../theme/icons/CartIcon";
import WishListIcon from "../../theme/icons/WishListIcon";

const Navbar = () => {
  return (
    <nav className="page-nav nav">
      <div className="container">
        <div className="nav-wrapper">
          <Link to="/" className="nav__logo">
            <BillboardIcon width='180px' />
            <span className="nav__logo-text"> charts</span>
          </Link>
          <ul className="right">
            <li>
              <Link to="/cart">
                <CartIcon width="35px" fill="#fff" />
              </Link>
            </li>
            <li>
              <Link to="/wishlist">
                <WishListIcon width="35px" fill="#fff" />
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
