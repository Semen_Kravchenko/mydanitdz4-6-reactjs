import React from "react";
import { Route, Switch } from "react-router";
import CartPage from "../pages/CartPage/CartPage";
import Error404Page from "../pages/Error404Page/Error404Page";
import HomePage from "../pages/HomePage/HomePage";
import WishListPage from "../pages/WishListPage/WishListPage";

const AppRoutes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <HomePage />
      </Route>
      <Route exact path="/cart">
        <CartPage />
      </Route>
      <Route exact path="/wishlist">
        <WishListPage />
      </Route>
      <Route path="*">
        <Error404Page />
      </Route>
    </Switch>
  );
};

export default AppRoutes;
