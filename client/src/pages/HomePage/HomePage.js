import React, { useEffect } from "react";
import PropTypes from "prop-types";
import "./HomePage.scss";
import { connect } from "react-redux";
import CardsToShow from "../../components/CardsToShow/CardsToShow";
import {
  closeModalOperation,
  getInfoOperation,
} from "../../store/operations/homeOperations";
import { addToWishListOperation } from "../../store/operations/wishListOperations";
import Loader from "../../components/Loader/Loader";
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/Button";
import { addToCartOperation } from "../../store/operations/cartOperations";

const HomePage = ({
  isLoading,
  getInfo,
  cardsInfo,
  addToCart,
  addToWishList,
  addedCardsFromCart,
  addedCardsFromWishList,
  isActiveModal,
  closeModal,
}) => {
  const checkModalId = isActiveModal.modalInfo.map((e) => e.id);
  const checkCartCardsId = addedCardsFromCart.map((e) => e.id);
  const checkWishListCardsId = addedCardsFromWishList.map((e) => e.id);

  useEffect(() => {
    if (!cardsInfo.length) getInfo();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (isLoading) {
    return <Loader />;
  }

  return (
    <>
      <section className="home-page home">
        <div className="container">
          <div className="home__inner">
            <ul className="home__cards-list">
              {cardsInfo.map((e) => (
                <CardsToShow
                  key={`card-to-show#${e.id}`}
                  id={e.id}
                  title={e.title}
                  author={e.author}
                  artNum={e.artNum}
                  price={e.price}
                  imgSrc={e.imgSrc}
                  addToCart={addToCart}
                  addToWishList={addToWishList}
                  addedCardsFromCart={addedCardsFromCart}
                  addedCardsFromWishList={addedCardsFromWishList}
                />
              ))}
            </ul>
          </div>
        </div>
      </section>
      {isActiveModal.isActive && (
        <Modal
          modalConfig={{
            header:
              checkModalId[0] === checkCartCardsId[0] ||
              checkModalId[0] === checkWishListCardsId[0]
                ? "Selected Item"
                : "Deleted Item",
            body: (
              <ul className="modal-list">
                {isActiveModal.modalInfo.map((e) => (
                  <CardsToShow
                    key={`modal#${e.id}`}
                    id={e.id}
                    title={e.title}
                    author={e.author}
                    artNum={e.artNum}
                    price={e.price}
                    imgSrc={e.imgSrc}
                  />
                ))}
              </ul>
            ),
            footer: (
              <div className="modal-button-wrapper">
                <Button onClick={() => closeModal()} className="modal-button">
                  OK
                </Button>
              </div>
            ),
            close: true
          }}
        />
      )}
    </>
  );
};

HomePage.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  getInfo: PropTypes.func.isRequired,
  cardsInfo: PropTypes.array.isRequired,
  addToCart: PropTypes.func.isRequired,
  addToWishList: PropTypes.func.isRequired,
  addedCardsFromCart: PropTypes.array.isRequired,
  addedCardsFromWishList: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => {
  return {
    isLoading: state.home.isLoading,
    cardsInfo: state.home.cardsInfo,
    isActiveModal: state.home.isActiveModal,
    addedCardsFromCart: state.cart.addedCards,
    addedCardsFromWishList: state.wishList.addedCards,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getInfo: () => dispatch(getInfoOperation()),
    addToCart: (id) => dispatch(addToCartOperation(id)),
    addToWishList: (id) => dispatch(addToWishListOperation(id)),
    closeModal: () => dispatch(closeModalOperation()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
