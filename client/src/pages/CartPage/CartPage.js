import React from "react";
import PropTypes from "prop-types";
import "./CartPage.scss";
import { connect } from "react-redux";
import AddedCardsToShow from "../../components/AddedCardsToShow/AddedCardsToShow";
import CheckoutForm from "../../components/CheckoutForm/CheckoutForm";
import {
  addQuantityOperation,
  openCheckOutOperation,
  removeCardOperation,
  subQuantityOperation,
} from "../../store/operations/cartOperations";
import Button from "../../components/Button/Button";

const CartPage = ({
  addedToCart,
  total,
  isCheckout,
  removeCard,
  addQuantity,
  subQuantity,
  openCheckOut,
}) => {
  if (isCheckout) {
    return <CheckoutForm />;
  }
  return (
    <section className="cart-page cart">
      <div className="container">
        <div className="cart__inner">
          <ul className="cart__added-cards-list">
            {!!addedToCart.length ? (
              addedToCart.map((e) => (
                <AddedCardsToShow
                  key={`added-card-to-show#${e.id}`}
                  label="cart"
                  id={e.id}
                  title={e.title}
                  author={e.author}
                  artNum={e.artNum}
                  imgSrc={e.imgSrc}
                  price={e.price}
                  removeCard={removeCard}
                  quantity={e.quantity}
                  subQuantity={subQuantity}
                  addQuantity={addQuantity}
                />
              ))
            ) : (
              <p className="cart__default-text">No items to show</p>
            )}
          </ul>
          <div className="cart__checkout">
            <div className="container">
              <div className="cart__checkout__inner">
                <p className="cart__total">Total: ${total}</p>
                <Button
                  disabled={!total}
                  onClick={() => openCheckOut(true)}
                  className="cart__checkout-button"
                >
                  Checkout
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

CartPage.propTypes = {
  addedToCart: PropTypes.array.isRequired,
  total: PropTypes.number.isRequired,
  isCheckout: PropTypes.bool,
  removeCard: PropTypes.func.isRequired,
  addQuantity: PropTypes.func.isRequired,
  subQuantity: PropTypes.func.isRequired,
  openCheckOut: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  return {
    addedToCart: state.cart.addedCards,
    total: state.cart.total,
    isCheckout: state.cart.isCheckout,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    removeCard: (id) => dispatch(removeCardOperation(id)),
    addQuantity: (id) => dispatch(addQuantityOperation(id)),
    subQuantity: (id) => dispatch(subQuantityOperation(id)),
    openCheckOut: (bool) => dispatch(openCheckOutOperation(bool)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartPage);
