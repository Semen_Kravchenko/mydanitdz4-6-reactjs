import React from "react";
import { Link } from "react-router-dom";
import "./Error404Page.scss";

const Error404Page = () => {
  return (
    <section className="error-404">
      <div className="container">
        <div className="error-404__inner">
          <p className="error-404__title">404</p>
          <p className="error-404__subtitle">Page not found</p>
          <div className="error-404__goToHome-btn-wrapper">
            <Link to="/" className="error-404__goToHome-btn">
              &#x25BA; Go to home page &#x25C4;
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Error404Page;
