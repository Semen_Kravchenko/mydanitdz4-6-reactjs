import React from "react";
import PropTypes from "prop-types";
import "./WishListPage.scss";
import { connect } from "react-redux";
import AddedCardsToShow from "../../components/AddedCardsToShow/AddedCardsToShow";
import { removeCardOperation } from "../../store/operations/wishListOperations";

const WishListPage = ({ addedToWishList, removeCard }) => {
  return (
    <section className="wishlist-page wishlist">
      <div className="container">
        <div className="wishlist__inner">
          <ul className="wishlist__added-cards-list">
            {!!addedToWishList.length ? (
              addedToWishList.map((e) => (
                <AddedCardsToShow
                  key={`added-card-to-show#${e.id}`}
                  label="cart"
                  id={e.id}
                  title={e.title}
                  author={e.author}
                  artNum={e.artNum}
                  imgSrc={e.imgSrc}
                  price={e.price}
                  removeCard={removeCard}
                />
              ))
            ) : (
              <p className="wishlist__default-text">No items to show</p>
            )}
          </ul>
        </div>
      </div>
    </section>
  );
};

WishListPage.propTypes = {
  addedToWishList: PropTypes.array.isRequired,
  removeCard: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  return {
    addedToWishList: state.wishList.addedCards,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    removeCard: (id) => dispatch(removeCardOperation(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(WishListPage);
