import subscribeState from "../../storage/subscribeState";
import {
  ADD_TO_WISHLIST,
  REMOVE_CARD_FROM_WISHLIST,
} from "../types/wishListTypes";

const initialState = subscribeState("wishListStorage", {
  addedCards: [],
  total: 0,
});

const wishListReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_WISHLIST:
      return { ...state, ...action.payload };
    case REMOVE_CARD_FROM_WISHLIST:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};

export default wishListReducer;
