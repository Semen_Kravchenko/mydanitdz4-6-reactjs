import { combineReducers } from "redux";
import cartReducer from "./cartReducer";
import homeReducer from "./homeReducer";
import wishListReducer from "./wishListReducer";

const rootReducer = combineReducers({
  home: homeReducer,
  cart: cartReducer,
  wishList: wishListReducer,
});

export default rootReducer;
