import {
  GET_INFO,
  SET_IS_ACTIVE_MODAL,
  SET_IS_LOADING,
} from "../types/homeTypes";

const initialState = {
  isLoading: false,
  cardsInfo: [],
  isActiveModal: {
    isActive: false,
    modalInfo: [],
  },
};

const homeReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_IS_LOADING:
      return { ...state, isLoading: true };
    case GET_INFO:
      return { ...state, isLoading: false, cardsInfo: action.payload };
    case SET_IS_ACTIVE_MODAL:
      return { ...state, isActiveModal: action.payload };
    default:
      return state;
  }
};

export default homeReducer;
