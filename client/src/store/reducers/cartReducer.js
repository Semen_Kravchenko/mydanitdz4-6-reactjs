import subscribeState from "../../storage/subscribeState";
import {
  ADD_QUANTITY,
  ADD_TO_CART,
  CLOSE_CHECK_OUT,
  OPEN_CHECK_OUT,
  REMOVE_CARD_FROM_CART,
  SUB_QUANTITY,
  CANCEL_CHECK_OUT,
} from "../types/cartTypes";

const initialState = subscribeState("cartStorage", {
  addedCards: [],
  total: 0,
  isCheckout: false,
});

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      return { ...state, ...action.payload };
    case REMOVE_CARD_FROM_CART:
      return { ...state, ...action.payload };
    case ADD_QUANTITY:
      return { ...state, ...action.payload };
    case SUB_QUANTITY:
      return { ...state, ...action.payload };
    case OPEN_CHECK_OUT:
      return { ...state, ...action.payload };
    case CLOSE_CHECK_OUT:
      return { ...state, ...action.payload };
    case CANCEL_CHECK_OUT:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};

export default cartReducer;
