const openModal = (id, initialState) => {
  const addedItem = initialState.cardsInfo.find((e) => e.id === id);
  return {
    ...initialState.isActiveModal,
    isActive: true,
    modalInfo: [addedItem],
  };
};

export default openModal;