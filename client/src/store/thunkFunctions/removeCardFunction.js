import addToStorage from "../../storage/addToStorage";

const removeCard = (id, mainState, isQuantity, storageName) => {
  const cardToRemove = mainState.addedCards.find((e) => e.id === id);
  const newCards = mainState.addedCards.filter((e) => e.id !== id);
  const totalAfterRemove = isQuantity
    ? mainState.total - cardToRemove.price * cardToRemove.quantity
    : mainState.total - cardToRemove.price;
  return addToStorage(storageName, {
    addedCards: newCards,
    total: +totalAfterRemove.toFixed(2),
  });
};

export default removeCard;
