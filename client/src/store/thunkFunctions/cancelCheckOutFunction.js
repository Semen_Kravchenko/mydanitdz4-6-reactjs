import addToStorage from "../../storage/addToStorage";

const cancelCheckOut = (bool, mainState, storageName) => {
  return addToStorage(storageName, {
    ...mainState,
    isCheckout: bool,
  });
};

export default cancelCheckOut;
