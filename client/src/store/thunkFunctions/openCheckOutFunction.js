import addToStorage from "../../storage/addToStorage";

const openCheckOut = (bool, mainState, storageName) => {
  return addToStorage(storageName, { ...mainState, isCheckout: bool });
};

export default openCheckOut;
