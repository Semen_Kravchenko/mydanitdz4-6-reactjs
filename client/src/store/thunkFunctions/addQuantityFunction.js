import addToStorage from "../../storage/addToStorage";

const addQuantity = (id, mainState, storageName) => {
  const addedItemWithAddQuantity = mainState.addedCards.find(
    (e) => e.id === id
  );
  addedItemWithAddQuantity.quantity += 1;
  const totalAfterAddQuantity =
    mainState.total + addedItemWithAddQuantity.price;
  return addToStorage(storageName, {
    addedCards: [...mainState.addedCards],
    total: +totalAfterAddQuantity.toFixed(2),
  });
};

export default addQuantity;
