const closeModal = (initialState) => {
  return {
    ...initialState.isActiveModal,
    isActive: false,
    modalInfo: [],
  };
};

export default closeModal;
