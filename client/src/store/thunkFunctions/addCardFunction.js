import addToStorage from "../../storage/addToStorage";

const addCard = (id, mainState, homeState, isQuantity, storageName) => {
  const addedCard = homeState.cardsInfo.find((e) => e.id === id);
  const existedCard = mainState.addedCards.find((e) => e.id === id);

  if (!!existedCard) {
    const newAddedCards = mainState.addedCards.filter(
      (e) => e.id !== existedCard.id
    );
    const newTotal = isQuantity
      ? mainState.total - existedCard.price * existedCard.quantity
      : mainState.total - existedCard.price;
    return addToStorage(storageName, {
      addedCards: [...newAddedCards],
      total: +newTotal.toFixed(2),
    });
  } else {
    const newTotal = mainState.total + addedCard.price;
    return addToStorage(storageName, {
      addedCards: [
        ...mainState.addedCards,
        isQuantity ? { ...addedCard, quantity: 1 } : { ...addedCard },
      ],
      total: +newTotal.toFixed(2),
    });
  }
};

export default addCard;
