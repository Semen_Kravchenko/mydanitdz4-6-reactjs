import addToStorage from "../../storage/addToStorage";

const closeCheckOut = (bool, mainState, storageName) => {
  const { addedCards, total } = mainState;
  const checkoutInfo = {
    itemCount: addedCards.length,
    items: addedCards,
    totalPrice: total,
  };
  console.log("Items info: ", checkoutInfo);
  return addToStorage(storageName, {
    addedCards: [],
    total: 0,
    isCheckout: bool,
  });
};

export default closeCheckOut;
