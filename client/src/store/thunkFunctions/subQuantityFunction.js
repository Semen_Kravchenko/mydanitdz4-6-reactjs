import addToStorage from "../../storage/addToStorage";

const subQuantity = (id, mainState, storageName) => {
  const addedItemWithSubQuantity = mainState.addedCards.find(
    (e) => e.id === id
  );
  if (addedItemWithSubQuantity.quantity === 1) {
    const newItems = mainState.addedCards.filter((e) => e.id !== id);
    const newTotal = mainState.total - addedItemWithSubQuantity.price;
    return addToStorage(storageName, {
      addedCards: newItems,
      total: +newTotal.toFixed(2),
    });
  } else {
    addedItemWithSubQuantity.quantity -= 1;
    const totalAfterSubQuantity =
      mainState.total - addedItemWithSubQuantity.price;
    return addToStorage(storageName, {
      addedCards: [...mainState.addedCards],
      total: +totalAfterSubQuantity.toFixed(2),
    });
  }
};

export default subQuantity;
