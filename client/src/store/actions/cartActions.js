import {
  ADD_TO_CART,
  REMOVE_CARD_FROM_CART,
  ADD_QUANTITY,
  SUB_QUANTITY,
  OPEN_CHECK_OUT,
  CLOSE_CHECK_OUT,
  CANCEL_CHECK_OUT,
} from "../types/cartTypes";

export const addToCartAction = (info) => ({ type: ADD_TO_CART, payload: info });
export const removeCardAction = (info) => ({
  type: REMOVE_CARD_FROM_CART,
  payload: info,
});
export const addQuantityAction = (info) => ({
  type: ADD_QUANTITY,
  payload: info,
});
export const subQuantityAction = (info) => ({
  type: SUB_QUANTITY,
  payload: info,
});
export const openCheckOutAction = (bool) => ({
  type: OPEN_CHECK_OUT,
  payload: bool,
});
export const closeCheckOutAction = (bool) => ({
  type: CLOSE_CHECK_OUT,
  payload: bool,
});
export const cancelCheckOutAction = (bool) => ({
  type: CANCEL_CHECK_OUT,
  payload: bool,
});