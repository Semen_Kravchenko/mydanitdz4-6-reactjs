import {
  GET_INFO,
  SET_IS_ACTIVE_MODAL,
  SET_IS_LOADING,
} from "../types/homeTypes";

export const setIsLoadingAction = () => ({ type: SET_IS_LOADING });
export const getInfoAction = (info) => ({ type: GET_INFO, payload: info });
export const setIsActiveModalAction = (info) => ({
  type: SET_IS_ACTIVE_MODAL,
  payload: info,
});
