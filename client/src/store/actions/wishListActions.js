import {
  ADD_TO_WISHLIST,
  REMOVE_CARD_FROM_WISHLIST,
} from "../types/wishListTypes";

export const addToWishListAction = (info) => ({
  type: ADD_TO_WISHLIST,
  payload: info,
});
export const removeCardAction = (info) => ({
  type: REMOVE_CARD_FROM_WISHLIST,
  payload: info,
});
