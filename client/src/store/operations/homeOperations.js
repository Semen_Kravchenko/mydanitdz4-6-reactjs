import axios from "axios";
import { getInfoAction, setIsActiveModalAction, setIsLoadingAction } from "../actions/homeActions";
import closeModal from "../thunkFunctions/closeModalFunction";

export const getInfoOperation = () => (dispatch) => {
  dispatch(setIsLoadingAction());
  setTimeout(() => {
    axios
      .get("/api/info")
      .then((info) => {
        dispatch(getInfoAction(info.data));
      })
      .catch((err) => console.log(err.message));
  }, 1000);
};

export const closeModalOperation = () => (dispatch, getState) => {
  const { home } = getState();
  dispatch(setIsActiveModalAction(closeModal(home)));
};
