import {
  addQuantityAction,
  addToCartAction,
  cancelCheckOutAction,
  closeCheckOutAction,
  openCheckOutAction,
  removeCardAction,
  subQuantityAction,
} from "../actions/cartActions";
import addCard from "../thunkFunctions/addCardFunction";
import addQuantity from "../thunkFunctions/addQuantityFunction";
import closeCheckOut from "../thunkFunctions/closeCheckOutFunction";
import openCheckOut from "../thunkFunctions/openCheckOutFunction";
import removeCard from "../thunkFunctions/removeCardFunction";
import subQuantity from "../thunkFunctions/subQuantityFunction";
import cancelCheckOut from "../thunkFunctions/cancelCheckOutFunction";
import openModal from "../thunkFunctions/openModalFunction";
import { setIsActiveModalAction } from "../actions/homeActions";

export const addToCartOperation = (id) => (dispatch, getState) => {
  const { home, cart } = getState();
  dispatch(addToCartAction(addCard(id, cart, home, true, "cartStorage")));
  dispatch(setIsActiveModalAction(openModal(id, home)));
};

export const removeCardOperation = (id) => (dispatch, getState) => {
  const { cart } = getState();
  dispatch(removeCardAction(removeCard(id, cart, true, "cartStorage")));
};

export const addQuantityOperation = (id) => (dispatch, getState) => {
  const { cart } = getState();
  dispatch(addQuantityAction(addQuantity(id, cart, "cartStorage")));
};

export const subQuantityOperation = (id) => (dispatch, getState) => {
  const { cart } = getState();
  dispatch(subQuantityAction(subQuantity(id, cart, "cartStorage")));
};

export const openCheckOutOperation = (bool) => (dispatch, getState) => {
  const { cart } = getState();
  dispatch(openCheckOutAction(openCheckOut(bool, cart, "cartStorage")));
};

export const closeCheckOutOperation = (bool) => (dispatch, getState) => {
  const { cart } = getState();
  dispatch(closeCheckOutAction(closeCheckOut(bool, cart, "cartStorage")));
};

export const cancelCheckOutOperation = (bool) => (dispatch, getState) => {
  const { cart } = getState();
  dispatch(cancelCheckOutAction(cancelCheckOut(bool, cart, "cartStorage")));
};
