import { setIsActiveModalAction } from "../actions/homeActions";
import {
  addToWishListAction,
  removeCardAction,
} from "../actions/wishListActions";
import addCard from "../thunkFunctions/addCardFunction";
import openModal from "../thunkFunctions/openModalFunction";
import removeCard from "../thunkFunctions/removeCardFunction";

export const addToWishListOperation = (id) => (dispatch, getState) => {
  const { home, wishList } = getState();
  dispatch(
    addToWishListAction(addCard(id, wishList, home, false, "wishListStorage"))
  );
  dispatch(setIsActiveModalAction(openModal(id, home)));
};

export const removeCardOperation = (id) => (dispatch, getState) => {
  const { wishList } = getState();
  dispatch(
    removeCardAction(removeCard(id, wishList, false, "wishListStorage"))
  );
};
