import React from "react";
import PropTypes from "prop-types";

const RemoveFromCartIcon = ({ width, fill, stroke }) => {
  return (
    <svg
      id="Capa_1"
      viewBox="0 0 512 512"
      width={width}
      fill={fill}
      stroke={stroke}
    >
      <g>
        <path d="m466.142 477.858c7.811 7.811 7.811 20.474 0 28.284-7.811 7.811-20.475 7.81-28.285 0l-25.857-25.857-25.857 25.857c-7.811 7.811-20.475 7.81-28.285 0-7.811-7.811-7.811-20.474 0-28.284l25.857-25.858-25.858-25.858c-7.811-7.811-7.811-20.474 0-28.284s20.475-7.811 28.285 0l25.858 25.857 25.857-25.857c7.811-7.811 20.475-7.811 28.285 0s7.811 20.474 0 28.284l-25.858 25.858zm-154.142 14.142c0 11.046-8.954 20-20 20h-232c-11.046 0-20-8.954-20-20v-352c0-11.046 8.954-20 20-20h60.946c7.944-67.478 65.477-120 135.054-120s127.109 52.522 135.054 120h60.946c11.046 0 20 8.954 20 20v192c0 11.046-8.954 20-20 20s-20-8.954-20-20v-172h-40v60c0 11.046-8.954 20-20 20s-20-8.954-20-20v-60h-192v60c0 11.046-8.954 20-20 20s-20-8.954-20-20v-60h-40v312h212c11.046 0 20 8.954 20 20zm-150.659-372h189.318c-7.64-45.345-47.176-80-94.659-80s-87.019 34.655-94.659 80z" />
      </g>
    </svg>
  );
};

RemoveFromCartIcon.propTypes = {
  width: PropTypes.string.isRequired,
  fill: PropTypes.string,
  stroke: PropTypes.string,
};

export default RemoveFromCartIcon;
