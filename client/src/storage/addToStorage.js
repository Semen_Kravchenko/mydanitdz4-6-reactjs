const addToStorage = (name = "", result) => {
  localStorage.setItem(name, JSON.stringify(result));
  return result;
};

export default addToStorage;
