const subscribeState = (storageName = "", state) => {
  const getFromStorage = JSON.parse(localStorage.getItem(storageName));
  if (!getFromStorage) {
    localStorage.setItem(storageName, JSON.stringify(state));
  } else if (getFromStorage !== state) {
    state = getFromStorage;
    return state;
  }
  return state;
};

export default subscribeState;
