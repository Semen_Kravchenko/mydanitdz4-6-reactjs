const express = require("express");
const api = require("./api/artistsInfo.json");
const app = express();

app.get("/api/info", (req, res) => {
  res.send(api.artists);
});

app.get("/api/info/:infoId", (req, res) => {
  const { infoId } = req.params;
  res.send(api.artists.find((e) => e.id === +infoId));
});

const port = 8080;

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
